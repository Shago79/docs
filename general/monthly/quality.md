# The role of the Quality team for a Monthly Release

## General process overview

Quality engineer helps facilitate the gating validations that happens during our regular monthly release process.
It is important to note that the Quality engineer is not the person responsible to verify every aspect of the release (bug fixes, features & etc).

This responsibility falls on everybody on the team:
* The developer who owns the Merge Request is responsible for validating the change/fix.
* We, GitLab Engineering and Product as a team, relies on collaboration and trust to work through the **Release Candidate QA Task** and check off all the items for a release.

Quality engineer works closely with Release Managers to ensure that the team checks off all the **Release Candidate QA Task** items for a given release.
These are done in:
* Monthly [Release Candidates QA task](../qa-issue-generation.md).

It is also helpful for the Quality Engineer to complete the Release Manager on-boarding and participate once as a trainee before taking part in the release process.

### Release Candidate QA tasks

A Release Manager will create a new QA issue for each new RC.

The process is automated, simply follow the instructions in [QA Issue Generation](../qa-issue-generation.md).

For whatever reason if the Release Manager is unable to do so, the Quality Engineer is the backup person.
